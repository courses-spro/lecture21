
import React from 'react';
import { loadStripe } from '@stripe/stripe-js';
import { Elements } from '@stripe/react-stripe-js';
import PaymentContainer from '../containers/payment';
const stripe = loadStripe('pk_test_51JZXD4D6sY8Eb1owFJhvUkEymickSIR1S5MgyxZHoFe5nJ3pYrjkRPdC29RdpqiwgN4fLkna6ROmpPfMOkxdNPOs00rvGl6RU9')
export default function PaymentPage() {
  return <Elements stripe={stripe}>
    <PaymentContainer/>
  </Elements>
}
