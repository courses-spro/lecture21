const { Router } = require('express');
const { handlePaymentWithToken } = require('./controller');

const router = Router();

router.post('/payment', handlePaymentWithToken);

module.exports = router
