const { payForOrderWithToken } = require('../util/stripe');

function handlePaymentWithToken(req, res) {
  const { body } = req;
  console.log(req)
  const response = payForOrderWithToken(body.order_id, body.user_id, body.token, body.amount);
  res.status(200).send(response);
}

module.exports = {
  handlePaymentWithToken
}
