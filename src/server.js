const Express = require('express');

const orderRouter = require('./order/router');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = new Express();
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(bodyParser.urlencoded());

app.use(cors());
app.use('/order', orderRouter);

app.listen(3001, () => {
  console.log('server is listening');
});
