const stripe = (require('stripe')('secret_key'));
const DEFAULT_CURRENCY = 'usd'

async function payForOrderWithToken(order_id, user_id, token, amount) {

  // const data = await stripe.customers.createSource(
  //   'cus_KE6vfkxABokEH0',
  //   {source: token}
  // );
  // console.log(data)
  return stripe.charges.create({
    source: 'card_1JZeojD6sY8Eb1owDr7rln3e',
    amount,
    currency: DEFAULT_CURRENCY,
    description: `user_id: ${user_id} paid ${amount} for order - ${order_id}`,
    customer: 'cus_KE6vfkxABokEH0'
  });
}

module.exports = {
  payForOrderWithToken
}
